import 'package:flutter_test/flutter_test.dart';
import 'package:aduro_app/services/api.dart';

void main() {
  test('API Service', () {
    ApiService service = ApiService();

    var lineChartData = service.getLineChartData();
    var pieChartData = service.getPieChartData();

    expect(lineChartData.length, 7);
    expect(pieChartData.length, 2);
  });
}
