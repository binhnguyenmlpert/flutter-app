import 'dart:math';
import 'package:flutter/material.dart';

class PieChartPainter extends CustomPainter {
  PieChartPainter({
    required this.strokeWidth,
    required this.innerPercent,
    required this.outerPercent,
  }) : super();

  final double strokeWidth;
  final double innerPercent;
  final double outerPercent;

  @override
  void paint(Canvas canvas, Size size) {
    double sizeMin = min(size.width, size.height);
    double radius = sizeMin / 2.0;
    double radius2 = radius * 2.0 / 3.0;
    Offset center = Offset(size.width / 2.0, size.height / 2.0);
    Rect rc = Rect.fromCircle(center: center, radius: radius);
    Rect rc2 = Rect.fromCircle(center: center, radius: radius2);

    var paint = Paint()
      ..strokeWidth = this.strokeWidth
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    // Outer
    paint.color = Color(0xffd9f8f3);
    canvas.drawCircle(center, radius, paint);

    paint.color = Color(0xff00c4a5);
    canvas.drawArc(
        rc, -pi / 2.0, this.outerPercent / 100.0 * 2.0 * pi, false, paint);

    // Inner
    paint.color = Color(0xffe0e9f2);
    canvas.drawCircle(center, radius2, paint);

    paint.color = Color(0xff184ea4);
    canvas.drawArc(
        rc2, -pi / 2.0, this.innerPercent / 100.0 * 2.0 * pi, false, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
