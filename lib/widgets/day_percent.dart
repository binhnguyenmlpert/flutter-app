import 'package:flutter/material.dart';
import 'package:aduro_app/widgets/pie_chart_painter.dart';

class DayPercent extends StatelessWidget {
  DayPercent({
    required this.name,
    required this.innerPercent,
    required this.outerPercent,
  }) : super();

  final String name;
  final double innerPercent;
  final double outerPercent;

  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Column(children: [
        CustomPaint(
          painter: PieChartPainter(
            strokeWidth: 2,
            innerPercent: this.innerPercent,
            outerPercent: this.outerPercent,
          ),
          child: Container(
            width: 20,
            height: 20,
          ),
        ),
        Text("$name"),
      ]),
    );
  }
}
