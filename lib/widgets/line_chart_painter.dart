import 'dart:math';
import 'package:flutter/material.dart';

class LineChartPainter extends CustomPainter {
  LineChartPainter({
    required this.dayData,
  }) : super();

  final List<double> dayData;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.translate(0.0, size.height);
    canvas.scale(1.0, -1.0);

    var paint = Paint()
      ..strokeWidth = 1
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    double top = size.height * 3 / 4.0;
    double spacing = 20;
    double colWidth = (size.width - spacing * 6) / 7;

    var rng = new Random();
    var days = [
      'M',
      'T',
      'W',
      'T',
      'F',
      'S',
      'S',
    ];

    paint.color = Color(0xff00c4a3);
    paint.style = PaintingStyle.fill;

    for (var i = 0; i < 7; i++) {
      Rect rc = Rect.fromLTWH(
        i * (colWidth + spacing),
        15,
        colWidth,
        dayData[i] / 100.0 * top,
      );

      canvas.drawRect(rc, paint);

      final textSpan = TextSpan(text: days[i]);
      final textPainter = TextPainter(
        text: textSpan,
        textDirection: TextDirection.ltr,
      );

      textPainter.layout(
        minWidth: 0,
        maxWidth: size.width,
      );

      final xCenter = i * (colWidth + spacing) + colWidth / 2.0;
      final yCenter = 10.0;

      canvas.save();
      canvas.translate(xCenter, yCenter);
      canvas.scale(1.0, -1.0);
      textPainter.paint(canvas, Offset(0.0, 0.0));
      canvas.restore();
    }

    paint.color = Color(0xffa0b3d1);

    var dashSize = size.width / 40;
    for (var i = 0; i < 20; i++) {
      canvas.drawLine(
        Offset(i * dashSize * 2, top),
        Offset(
          i * dashSize * 2 + dashSize,
          top,
        ),
        paint,
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
