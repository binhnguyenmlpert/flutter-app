import 'package:flutter/material.dart';
import 'package:aduro_app/widgets/pie_chart_painter.dart';
import 'package:aduro_app/services/api.dart';

class Header extends StatefulWidget {
  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  ApiService service = ApiService();

  @override
  Widget build(BuildContext context) {
    var dayInfo = service.getDayInfo();
    var today = service.getPieChartData();

    return Column(children: [
      Center(
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: [
            CustomPaint(
              painter: PieChartPainter(
                strokeWidth: 5,
                innerPercent: today[0],
                outerPercent: today[1],
              ),
              child: Container(
                width: 100,
                height: 100,
              ),
            ),
            Column(children: <Widget>[
              Text(
                "16",
                style: TextStyle(
                  fontSize: 25,
                  color: Color(0xff00c4a5),
                ),
              ),
              Text(
                "444",
                style: TextStyle(
                  fontSize: 15,
                  color: Color(0xff184ea4),
                ),
              ),
            ]),
          ],
        ),
      ),
      Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(
                  Icons.favorite,
                  color: Color(0xff00c4a5),
                ),
                Text("Heart Pts",
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ],
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.settings,
                  color: Color(0xff184ea4),
                ),
                Text(
                  "Steps",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
      Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(
                  "${dayInfo.cal}",
                  style: TextStyle(
                    color: Color(0xff1d74dd),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text("Cal"),
              ],
            ),
            Column(
              children: <Widget>[
                Text("${dayInfo.km}",
                    style: TextStyle(
                      color: Color(0xff1d74dd),
                      fontWeight: FontWeight.bold,
                    )),
                Text("Km"),
              ],
            ),
            Column(
              children: <Widget>[
                Text("${dayInfo.moveMin}",
                    style: TextStyle(
                      color: Color(0xff1d74dd),
                      fontWeight: FontWeight.bold,
                    )),
                Text("Move Min"),
              ],
            ),
          ],
        ),
      ),
    ]);
  }
}
