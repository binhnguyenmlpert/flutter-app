import 'package:flutter/material.dart';
import 'package:aduro_app/widgets/header.dart';
import 'package:aduro_app/widgets/line_chart_painter.dart';
import 'package:aduro_app/widgets/day_percent.dart';
import 'package:aduro_app/services/api.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ApiService service = ApiService();

  @override
  Widget build(BuildContext context) {
    var mon = service.getPieChartData();
    var tue = service.getPieChartData();
    var wed = service.getPieChartData();
    var thu = service.getPieChartData();
    var fri = service.getPieChartData();
    var sat = service.getPieChartData();
    var sun = service.getPieChartData();

    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.paste),
            label: 'Journal',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: Header(),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Card(
                  child: Column(
                    children: [
                      ButtonBar(
                        alignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Your daily goals",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              children: [
                                Text("0/7"),
                                Text("Achieved"),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                DayPercent(
                                  name: "M",
                                  innerPercent: mon[0],
                                  outerPercent: mon[1],
                                ),
                                DayPercent(
                                  name: "T",
                                  innerPercent: tue[0],
                                  outerPercent: tue[1],
                                ),
                                DayPercent(
                                  name: "W",
                                  innerPercent: wed[0],
                                  outerPercent: wed[1],
                                ),
                                DayPercent(
                                  name: "T",
                                  innerPercent: thu[0],
                                  outerPercent: thu[1],
                                ),
                                DayPercent(
                                  name: "F",
                                  innerPercent: fri[0],
                                  outerPercent: fri[1],
                                ),
                                DayPercent(
                                  name: "S",
                                  innerPercent: sat[0],
                                  outerPercent: sat[1],
                                ),
                                DayPercent(
                                  name: "S",
                                  innerPercent: sun[0],
                                  outerPercent: sun[1],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  child: Column(
                    children: <Widget>[
                      ButtonBar(
                        alignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Sleep Duration",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              "No recent data",
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  child: Column(
                    children: <Widget>[
                      ButtonBar(
                        alignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Heart Points",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(right: 20.0),
                              child: Column(
                                children: [
                                  Text(
                                    "0",
                                    style: TextStyle(
                                      fontSize: 35,
                                      color: Color(0xff244a8b),
                                    ),
                                  ),
                                  Text(
                                    "Today",
                                    style: TextStyle(
                                      color: Color(0xff244a8b),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: CustomPaint(
                                painter: LineChartPainter(
                                    dayData: service.getLineChartData()),
                                child: Container(
                                  height: 120,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
