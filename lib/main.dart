import 'package:flutter/material.dart';
import 'package:aduro_app/pages/login_page.dart';
import 'package:aduro_app/pages/home_page.dart';

void main() {
  runApp(AduroApp());
}

class AduroApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => LoginPage(),
        '/home': (context) => HomePage(),
      },
    );
  }
}
