import 'dart:math';

class DayInfo {
  DayInfo({
    required this.cal,
    required this.km,
    required this.moveMin,
  });

  final int cal;
  final double km;
  final int moveMin;
}

class ApiService {
  DayInfo getDayInfo() {
    return DayInfo(cal: 1090, km: 0.5, moveMin: 21);
  }

  List<double> getPieChartData() {
    var rng = new Random();

    var percents = [
      rng.nextDouble() * 100.0,
      rng.nextDouble() * 100.0,
    ];

    return percents;
  }

  List<double> getLineChartData() {
    var rng = new Random();

    var percents = [
      rng.nextDouble() * 100.0,
      rng.nextDouble() * 100.0,
      rng.nextDouble() * 100.0,
      rng.nextDouble() * 100.0,
      rng.nextDouble() * 100.0,
      rng.nextDouble() * 100.0,
      rng.nextDouble() * 100.0,
    ];

    return percents;
  }
}
